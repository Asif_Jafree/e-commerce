// import { useEffect, useState } from 'react';
import React from 'react';
import { BrowserRouter as Switch, Route } from 'react-router-dom';
import Home from './Component/Home';
import ProductDetails from './Component/ProductDetails';
import AllProduct from './Component/AllProduct';
import Signup from './Component/Signup';
import Login from './Component/Login';
import About from './Component/About';
import Contact from './Component/Contact';
import CartItem from './Component/CartItem';
import BuyProduct from './Component/BuyProduct';
import Delivery from './Component/Delivery';

function App(props) {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route
        exact
        path="/productdetails/:id"
        component={() => <ProductDetails />}
      />
      <Route exact path="/Product" component={AllProduct} />
      <Route exact path="/signup" component={Signup} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/About" component={About} />
      <Route exact path="/Contact" component={Contact} />
      <Route exact path="/cartitem" component={CartItem} />
      <Route exact path="/buyproduct" component={BuyProduct} />
      <Route exact path="/delivery" component={Delivery} />
    </Switch>
  );
}

export default App;
