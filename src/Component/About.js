import React from 'react';
import Footer from './Footer';
import Navbar from './Navbar';
import photo from '../Image/shopping2.jpg'

function About() {
  return (
    <div className="About">
      <Navbar />
      <div className='p-4'>
      <div className='flex h-96 p-4'>
      <img className="w-2/4" src={photo} alt='' />
      <p className='p-20 text-orange-700'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
         tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
         veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea</p>
      </div>
      </div>
      <Footer/>
      </div>
      );
    }
    export default About;
    