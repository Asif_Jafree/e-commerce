import React from 'react';
import Navbar from './Navbar';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Footer from './Footer';

export default function AllProduct() {
  const [product, setProduct] = useState([]);
  const [result, setResult] = useState([]);
  const [loder, setLoder] = useState(true);

  const findProduct = () => {
    fetch('https://fakestoreapi.com/products')
      .then((r) => r.json())
      .then((data) => {
        setProduct(data);
        setResult(data);
        setLoder(false);
      });
  };
  useEffect(() => {
    findProduct();
  }, []);
  return (
    <div>
      <Navbar />
      {!loder ? (
        <div className="flex flex-wrap justify-center">
          {result.map((ele) => (
            <div className="w-[250px] hover:shadow-lg  m-3 transform transition duration-500 hover:scale-105 rounded-lg border-[1px] hover:border-2 hover:border-[#D1D0CE] border-[#D1D0CE] cursor-pointer">
              <Link to={{ pathname: `/productdetails/${ele.id}` }}>
                <img
                  src={ele.image}
                  alt=""
                  className="w-[100%] h-[300px] rounded-lg"
                />
                <h1 className="text-xl text-center">
                  {ele.title.slice(0, 12)}...
                </h1>
                <p className="text-xl text-center">Price: ${ele.price}</p>
              </Link>
            </div>
          ))}
        </div>
      ) : (
        <p className="text-center text-fuchsia-900 mb-10 text-2xl italic">
          Keep smile fashion is loding...
        </p>
      )}

      {!loder?<Footer />:''}
    </div>
  );
}
