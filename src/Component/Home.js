import React, { useState } from 'react';
import Navbar from './Navbar';
import Photo from './Photo';
import Product from './Product';

export default function Home(props) {
  return (
    <div>
      <Navbar loginData={props.loginData} />
      <Photo />
      <Product />
    </div>
  );
}
