import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';

//  localStorage.setItem('users', '[]');

// let arr = [];
// if (localStorage.getItem('users').length === 0) {
//   localStorage.setItem('users', '[]');
// } else {
//   // [...JSON.parse(localStorage['users'])];
// }

export default function Signup() {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    phone: '',
    password: '',
    checked: 'of',
  });
  let history = useHistory();
  const [checkName, setCheckName] = useState(true);
  const [checkEmail, setCheckEmail] = useState(true);
  const [checkPhone, setCheckPhone] = useState(true);
  const [checkPassword, setCheckPassword] = useState(true);
  const [recheckPassword, setreCheckPassword] = useState(true);
  const [condition, setCondition] = useState(true);
  const [message, setMessage] = useState('*Please enter a valid email');

  const validateName = (input) => {
    if (input.length < 3 || input[0] === ' ') {
      setCheckName(false);
      return false;
    } else {
      setCheckName(true);
      return true;
    }
  };
  const validateEmail = (input) => {
    if (input.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
      setCheckEmail(true);
      return true;
    } else {
      setCheckEmail(false);
      return false;
    }
  };
  const validatePhone = (input) => {
    if (input.length === 10) {
      setCheckPhone(true);
      return true;
    } else {
      setCheckPhone(false);
      return false;
    }
  };
  const validatePassword = (input) => {
    if (input.length > 5 && !input.includes(' ')) {
      setCheckPassword(true);
      return true;
    } else {
      setCheckPassword(false);
      return false;
    }
  };
  const validateConfirmPassword = (input) => {
    if (formData.confirmPassword !== formData.password) {
      setreCheckPassword(false);
      return false;
    } else {
      setreCheckPassword(true);
      return true;
    }
  };
  const validateCondition = (input) => {
    console.log(input);
    if (input === 'of') {
      setCondition(false);
      return false;
    } else {
      setCondition(true);
      return true;
    }
  };

  const handleSubmit = () => {
    if (
      !(
        validateName(formData.name) &&
        validateEmail(formData.email) &&
        validatePhone(formData.phone) &&
        validatePassword(formData.password) &&
        validateConfirmPassword(formData.confirmPassword) &&
        validateCondition(formData.checked)
      )
    ) {
      return;
    }

    // for (let index = 0; index < arr.length; index++) {
    //   if (arr[index].email === formData.email) {
    //     setMessage('Email already exist');
    //     setCheckEmail(false);
    //     console.log('*Email exist');
    //     return;
    //   }
    // }
    // arr = [...arr, formData];
    localStorage.setItem('users', JSON.stringify(formData));
    let path = './login';
    history.push(path);
  };
  const handleUserInput = (event) => {
    let value = event.target.value;
    if (formData.checked === 'of' && event.target.name === 'checked') {
      value = 'on';
    } else if (formData.checked === 'on' && event.target.name === 'checked') {
      value = 'of';
    }
    let key = event.target.name;
    setFormData({ ...formData, [key]: value });
  };
  console.log(formData);
  return (
    <div className="bg-[#d0cfcf] h-[fit-content] ">
      <div className="flex justify-center">
        <div className="flex flex-col items-center shadow-lg w-[500px] space-y-4 bg-white p-3 mt-10 justify-center">
          <div>
            <h1 className="text-3xl">SignUp</h1>
          </div>
          <div className="flex flex-col items-center space-y-2">
            <label>Enter your name</label>
            <input
              type="text"
              placeholder="Enter your name"
              name="name"
              className="border-2 border-gray-400 focus:border-blue-500 w-[400px] p-1 outline-none rounded-sm"
              onChange={(event) => handleUserInput(event)}
              required
            />
            <p
              style={{
                display: checkName ? 'none' : 'contents',
                color: 'red',
                fontSize: "14px",
              }}
            >
              *Name conatains atleast 3 letter
            </p>
          </div>
          <div className="flex flex-col items-center space-y-2">
            <label>Enter your email</label>
            <input
              type="email"
              placeholder="Enter your email"
              name="email"
              className="border-2 outline-none focus:border-blue-500 border-gray-400 w-[400px] p-1 rounded-sm"
              onChange={(event) => handleUserInput(event)}
              required
            />
            <p
              style={{
                display: checkEmail ? 'none' : 'contents',
                color: 'red',
                fontSize: "14px",
              }}
            >
              {message}
            </p>
          </div>
          <div className="flex flex-col items-center space-y-2">
            <label>Enter your number</label>
            <input
              type="number"
              placeholder="Enter your number"
              name="phone"
              className="border-2 outline-none focus:border-blue-500 border-gray-400 w-[400px] p-1 rounded-sm"
              onChange={(event) => handleUserInput(event)}
              required
            />
            <p
              style={{
                display: checkPhone ? 'none' : 'contents',
                color: 'red',
                fontSize: "14px",
              }}
            >
              *Please Insert 10 digit number
            </p>
          </div>
          <div className="flex flex-col items-center space-y-2">
            <label>Enter your password</label>
            <input
              type="password"
              placeholder="Enter your password"
              name="password"
              className="border-2 outline-none focus:border-blue-500 border-gray-400 w-[400px] p-1 rounded-sm"
              onChange={(event) => handleUserInput(event)}
              required
            />
            <p
              style={{
                display: checkPassword ? 'none' : 'contents',
                color: 'red',
                fontSize: "14px",
              }}
            >
              *Password conatains atleast 6 letter without space
            </p>
          </div>
          <div className="flex flex-col items-center space-y-2">
            <label>Enter your confirm password</label>
            <input
              type="password"
              name="confirmPassword"
              placeholder="ReEnter your password"
              className="border-2 outline-none focus:border-blue-500 border-gray-400 w-[400px] p-1 rounded-sm"
              onChange={(event) => handleUserInput(event)}
            />
            <p
              style={{
                display: recheckPassword ? 'none' : 'contents',
                color: 'red',
                fontSize: "14px",
              }}
            >
              *Password not matched
            </p>
          </div>
          <div className="flex flex-col justify-start items-center">
            <div>
              <input
                type="checkbox"
                name="checked"
                onChange={(event) => handleUserInput(event)}
              />
              <label>Agree the terms and conditions</label>
            </div>
            <div>
              <p
                style={{
                  display: condition ? 'none' : 'contents',
                  color: 'red',
                  fontSize: "14px",
                }}
              >
                *Please accept condition
              </p>
            </div>
          </div>
          <div className="">
            <input
              type="submit"
              value="Register"
              className="text-white text-xl cursor-pointer rounded-xl bg-pink-600 px-8 py-2 focus:border-none focus:outline-none border-none"
              onClick={() => handleSubmit()}
            />
          </div>
          <div className="">
            <p>
              Existing User?{' '}
              <Link to="/login" className="text-blue-600">
                Login
              </Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
