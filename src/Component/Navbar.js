import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function Navbar(props) {
  const user = JSON.parse(localStorage.getItem('users'));

  const result = JSON.parse(localStorage.getItem('cartItem'));
  const [users, setUser] = useState(user);

  const deleteUser = () => {
    const res = localStorage.removeItem('users');
    setUser(res);
  };

  return (
    <div className="flex justify-between items-center ">
      <div className="flex justify-center items-center">
        <h3 className="text-[40px] text-amber-900">
          <span className="text-[60px]">F</span>ashion{' '}
          <span className="text-[60px]">S</span>hopee
        </h3>
      </div>
      <ul className="flex justify-center items-center space-x-5 mt-5 text-xl ">
        <li className="hover:border-blue-500 hover:border-b-2 pb-4">
          <Link to="/">Home</Link>
        </li>
        <li className="hover:border-blue-500 hover:border-b-2 pb-4">
          <Link to="/Product">Product</Link>
        </li>
        <li className="hover:border-blue-500 hover:border-b-2 pb-4">
          <Link to="/About">About</Link>
        </li>
        <li className="hover:border-blue-500 hover:border-b-2 pb-4">
          <Link to="/Contact">Contact</Link>
        </li>
      </ul>
      <div className="flex justify-center items-center space-x-4">
        {!users ? (
          <div className="flex justify-between space-x-4">
            <Link to="/login">
              <button className="border-2 border-gray-400 rounded-lg p-2">
                <i className="fa-solid fa-arrow-right-to-bracket"></i> Login
              </button>
            </Link>
            <Link to="/signup">
              <button className="border-2 border-gray-400 rounded-lg p-2">
                <i className="fa-solid fa-user-plus"></i> Register
              </button>
            </Link>
          </div>
        ) : (
          <div className="flex items-center space-x-4">
            <div className="border-2 border-gray-400 rounded-lg p-2">
              {users.name}
            </div>
            <button
              onClick={deleteUser}
              className="border-2 border-gray-400 rounded-lg p-2"
            >
              Log out
            </button>
          </div>
        )}

        <Link to="/cartitem">
          <button className="border-2 border-gray-400 rounded-lg p-2">
            <i className="fa-solid fa-cart-shopping"></i>
            {result.length > 0 ? <span className="">{result.length}</span> : ''}
          </button>
        </Link>
      </div>
    </div>
  );
}
export default Navbar;
