import React from 'react';

export default function Delivery() {
  return (
    <div className='flex flex-col justify-center items-center mt-32 '>
      <h1 className='text-4xl text-green-800'>Thank you</h1>
      <p className='text-2xl text-blue-800 mt-10'> Happy to serve you,continue shopping with us</p>
    </div>
  );
}
