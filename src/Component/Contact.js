import React from 'react';
import Footer from './Footer';
import Navbar from './Navbar';

function Contact() {
  return (
    <div className="Contact">
    <Navbar />
    <div className="flex flex-col items-center p-16 bg-hero-pattern bg-right-bottom text-xl">
      <p className='text-3xl text-red-700 m-5 p-4'>Contact Us .....</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
      veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
      commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
      velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
      occaecat cupidatat non proident, sunt in culpa qui officia deserunt
      mollit anim id est laborum.
      <br />
      “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua.”</p>
    </div>
    <Footer/>
    </div>
  );
}
export default Contact;
