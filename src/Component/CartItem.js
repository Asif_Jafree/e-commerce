import React from 'react';
import { useEffect, useState } from 'react';
import Navbar from './Navbar';

export default function CartItem() {
  let arr = [...JSON.parse(localStorage.getItem('cartItem'))];
  const [product, setProduct] = useState([]);
  const [cartProduct, setCartProduct] = useState([]);
  const [isLoded, setLoder] = useState(true);

  const findProduct = () => {
    let cart = [];
    fetch('https://fakestoreapi.com/products')
      .then((r) => r.json())
      .then((data) => {
        setProduct(data);
        for (let index = 0; index < arr.length; index++) {
          for (let inner = 0; inner < data.length; inner++) {
            if (Number(arr[index].value) === data[inner].id) {
              data[inner]['qty'] = arr[index].count;
              cart.push(data[inner]);
              setLoder(false);
            }
          }
        }

        localStorage.setItem('productIncart', JSON.stringify(cart));
        setCartProduct(JSON.parse(localStorage.getItem('productIncart')));
      });
  };
  useEffect(() => {
    findProduct();
  }, []);

  console.log('Cart Product', cartProduct);

  const handleIncrese = (val) => {
    let showCart = cartProduct.map((ele) => {
      if (ele.id === val) {
        ele.qty += 1;

        arr[arr.findIndex((pro) => pro.value == val)].count += 1;
        localStorage.setItem('cartItem', JSON.stringify(arr));
      }
      return ele;
    });
    setCartProduct(showCart);
    localStorage.setItem('productIncart', JSON.stringify(showCart));
  };

  const handleDecrese = (val) => {
    let showCart = cartProduct.map((ele) => {
      if (ele.qty < 1) return ele;
      if (ele.id === val) {
        ele.qty -= 1;

        arr[arr.findIndex((pro) => pro.value == val)].count -= 1;
        localStorage.setItem('cartItem', JSON.stringify(arr));
        console.log(arr);
      }
      return ele;
    });
    setCartProduct(showCart);
    localStorage.setItem('productIncart', JSON.stringify(showCart));
  };

  const handleDelete = (val) => {
    console.log('delete');
    let showCart = cartProduct.map((ele) => {
      if (ele.id === val) {
        let index = arr.findIndex((pro) => pro.value == val);
        console.log(index);
        arr.splice(index, 1);
        console.log(arr);
        localStorage.setItem('cartItem', JSON.stringify(arr));
        console.log(arr);
      }
      return ele;
    });
    setCartProduct(showCart);
    localStorage.setItem('productIncart', JSON.stringify(showCart));
    findProduct();
  };

  return (
    <>
      <Navbar />
      {!isLoded ? (
        <div className="flex flex-col">
          {cartProduct.length > 0 ? (
            cartProduct.map((ele) => (
              <div className="flex items-center ml-20 space-x-10 m-4 ">
                <div>
                  <img src={ele.image} alt="Items" className="w-[200px]" />
                </div>
                <div className="flex flex-col space-y-5">
                  <p>{ele.title}</p>
                  <p>
                    Rating
                    <span>
                      {ele?.rating?.rate}
                      <i class="fa fa-star"></i>
                    </span>
                  </p>
                  <div className="flex space-x-4">
                    <div className="flex items-center  ">
                      <button
                        className="bg-gray-400  outline-none border-none p-[10px]"
                        onClick={() => handleIncrese(ele.id)}
                      >
                        +
                      </button>
                      <p className="border-2 border-gray-400 p-2">{ele.qty}</p>
                      <button
                        className="bg-gray-400 outline-none border-none p-[10px]"
                        onClick={() => handleDecrese(ele.id)}
                      >
                        -
                      </button>
                    </div>
                    <div className="flex space-x-4 items-center">
                      <button
                        className="bg-red-500 rounded p-2 text-white"
                        onClick={() => handleDelete(ele.id)}
                      >
                        Delete Item
                      </button>
                      <p>
                        Total Price: $
                        <span className="font-bold">
                          {(ele.price * ele.qty).toFixed(2)}
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            ))
          ) : (
            <p className="text-center font-bold  text-2xl my-52 ">
              Your Cart is empty
            </p>
          )}
        </div>
      ) : (
        <p className="text-center text-fuchsia-900 mb-10 text-2xl italic">
          Your product is loading...
        </p>
      )}
    </>
  );
}
