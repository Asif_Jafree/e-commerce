import React from 'react';
import { useParams, Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Navbar from './Navbar';
import photo from '../Image/addProduct.png';

let arr = [];
if (!localStorage.getItem('cartItems')) localStorage.setItem('cartItem', '[]');
if (localStorage.length === 0) {
  localStorage.setItem('cartItem', '[]');
} else {
  arr = [...JSON.parse(localStorage['cartItem'])];
}
localStorage.setItem('productIncart', '[]');

export default function ProductDetails(props) {
  const params = useParams();
  // console.log(params.id);

  const [product, setProduct] = useState([]);
  const [cartData, setCartData] = useState([]);
  const [loder, setLoder] = useState(true);

  const findProduct = () => {
    fetch('https://fakestoreapi.com/products/' + params.id)
      .then((r) => r.json())
      .then((data) => {
        setProduct(data);
        setLoder(false);
      });
  };

  useEffect(() => {
    findProduct();
  }, []);

  const addProduct = () => {
    arr = [...JSON.parse(localStorage['cartItem'])];
    if (arr.findIndex((ele) => ele.value == params.id) > -1) {
      return;
    }
    let count = { value: params.id, count: 1 };
    arr = [...arr, count];
    console.log(arr);
    localStorage.setItem('cartItem', JSON.stringify(arr));
    const res = JSON.parse(localStorage.getItem('cartItem'));
    setCartData(res);
  };

  return (
    <>
      <Navbar cartData={cartData} />
      {!loder ? (
        <div className="flex justify-center items-center mt-10">
          <div>
            <img
              src={product.image}
              alt="product"
              className="w-[500px] h-[500px]"
            />
          </div>
          <div className="flex flex-col justify-between pl-28 w-[50%] h-[500px]">
            <h1 className="uppercase text-gray-600 text-3xl">
              {product.category}
            </h1>
            <h1 className="text-[45px] text-gray-700">{product.title}</h1>
            <p className="font-semibold text-lg">
              Rating {product?.rating?.rate} <i class="fa fa-star"></i>
            </p>
            <p className="text-2xl ">{product.description}</p>
            <p className="font-bold text-[40px]">${product.price}</p>
            <div className="flex">
              <button
                className="border-none p-2 mr-5 rounded-lg"
                onClick={addProduct}
              >
                <img src={photo} alt="Add -to-cart" />
              </button>
              <Link to="/buyproduct">
                <button className=" bg-yellow-500 text-white p-3 mt-2 rounded-lg">
                  Buy Now
                </button>
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <p className="text-center text-fuchsia-900 mb-10 text-2xl italic">Your product is loading...</p>
      )}
    </>
  );
}
