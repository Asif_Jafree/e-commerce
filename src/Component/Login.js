import React, { useState } from 'react';
import { Link } from 'react-router-dom';

export default function Login(props) {
  const [data, setData] = useState({ email: '', password: '' });
  const [checkPassword, setPassword] = useState(true);
  const [checkEmail, setEmail] = useState(true);
  const [credential, setCredential] = useState(false);

  const handleUserInput = (event) => {
    let value = event.target.value;
    let key = event.target.name;
    setData({ ...data, [key]: value });
  };

  const handleSubmit = () => {
    const user = JSON.parse(localStorage['users']);
    //let credential = false;
    if (user.email === data.email && user.password === data.password) {
      setCredential(true);
    } else if (user.email !== data.email) {
      setEmail(false);
    } else if (user.password !== data.password) {
      setEmail(true);
      setPassword(false);
    }
  };
  return (
    <div className="bg-[#d0cfcf] h-[100vh] ">
      <div className="flex justify-center">
        <div className="flex flex-col items-center shadow-lg w-[500px] space-y-10 bg-white p-3 mt-10 justify-center">
          <div>
            <h1 className="text-3xl">Login</h1>
          </div>

          <div className="flex flex-col items-center space-y-3">
            <label>Enter your email</label>
            <input
              type="email"
              name="email"
              placeholder="Enter your email"
              className="border-2 outline-none focus:border-blue-500 border-gray-400 w-[400px] p-1 rounded-sm"
              required
              onChange={(event) => handleUserInput(event)}
            />
            <p
              style={{
                display: checkEmail ? 'none' : 'contents',
                color: 'red',
                fontSize: '10px',
              }}
            >
              *Email not exist
            </p>
          </div>

          <div className="flex flex-col items-center space-y-3">
            <label>Enter your password</label>
            <input
              type="password"
              name="password"
              placeholder="Enter your password"
              className="border-2 outline-none focus:border-blue-500 border-gray-400 w-[400px] p-1 rounded-sm"
              onChange={(event) => handleUserInput(event)}
            />
            <p
              style={{
                display: checkPassword ? 'none' : 'contents',
                color: 'red',
                fontSize: '10px',
              }}
            >
              *Password not Matched
            </p>
          </div>

          <div className="bg-pink-600 px-8 py-4 rounded-lg">
            {credential ? (
              <Link to="/">
                <input
                  type="submit"
                  value="Login"
                  className="text-white text-xl border-none outline-none
                   hover:border-none hover:outline-none cursor-pointer"
                  onClick={() => handleSubmit()}
                />
              </Link>
            ) : (
              <input
                type="submit"
                value="Login"
                className="text-white text-xl border-none outline-none hover:border-none hover:outline-none cursor-pointer"
                onClick={() => handleSubmit()}
              />
            )}
          </div>
          <div>
            <p>
              New to Fashion Shopee?{' '}
              <Link to="/signup" className="text-blue-500">
                Create an account
              </Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
