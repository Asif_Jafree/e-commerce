import React from 'react';
import { Link } from 'react-router-dom';
import Navbar from './Navbar';

export default function BuyProduct() {
  return (
    <div>
      <Navbar />
      <div className="flex justify-center bg-[#d0cfcf] h-[fit-content]">
        <div className="flex flex-col shadow-lg w-[500px] space-y-4 bg-white p-3 mt-10 justify-center items-center">
          <div>
            <h1 className="text-3xl">Address</h1>
          </div>
          <div className="flex flex-col items-center space-y-2">
            <label>Full name</label>
            <input
              type="text"
              placeholder="Enter your first name"
              className="border-2 border-gray-400 focus:border-blue-500 w-[400px] p-1 outline-none rounded-sm"
            />
          </div>
          <div className="flex flex-col items-center space-y-2">
            <label>Mobile Number</label>
            <input
              type="text"
              placeholder="10 digit mobile number without prifix"
              className="border-2 border-gray-400 focus:border-blue-500 w-[400px] p-1 outline-none rounded-sm"
            />
          </div>
          <div className="flex flex-col items-center space-y-2">
            <label>Pincode</label>
            <input
              type="number"
              placeholder="6 digit[0-9] PIN code"
              className="border-2 border-gray-400 focus:border-blue-500 w-[400px] p-1 outline-none rounded-sm"
            />
          </div>
          <div className="flex flex-col items-center space-y-2">
            <label>Flat,Company,Apartment</label>
            <input
              type="text"
              placeholder="Flat,Company,Apartment"
              className="border-2 border-gray-400 focus:border-blue-500 w-[400px] p-1 outline-none rounded-sm"
            />
          </div>
          <div className="flex flex-col items-center space-y-2">
            <label>Area</label>
            <input
              type="text"
              placeholder="Enter your Locality"
              className="border-2 border-gray-400 focus:border-blue-500 w-[400px] p-1 outline-none rounded-sm"
            />
          </div>
          <Link to='/delivery'>
          <div className="">
            <input
              type="submit"
              value="Checkout"
              className="text-white text-xl cursor-pointer rounded-xl bg-pink-600 px-8 py-2 focus:border-none focus:outline-none border-none"
            />
          </div>
          </Link>
        </div>
      </div>
    </div>
  );
}
