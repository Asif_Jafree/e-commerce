module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}"
  ],
  theme: {
    extend: {
      backgroundImage: {
        'hero-pattern': "url('./Image/shopping7.jpg')",
        'footer-texture': "url('./Image/shopping5.png')",
      }
    }
  },
  plugins: [],
}
